FROM node:lts-alpine3.20
WORKDIR /app
COPY . /app
RUN npm install
