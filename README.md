# NOTES
## System Design
### AWS Networks
- Designed to use three tier architecture which include public tier, app tier, and data tier
- Public tier contains public subnet and internet gateway.
  - Resources created here will be mapped with public IP
  - The implemented availability zone is 2 zone, in real scenario, its recommended to use the available AZ in that particular regions.
- App tier and data tier contains private subnets and using NAT Gateway to communicate to the Internet.
  - The implemented availability zone is 2 zone, in real scenario, its recommended to use the available AZ in that particular regions.
  - NAT GW implemented only on single AZ, in real scenario, its recommended to create NGW on each available AZ in that particular regions.

For the diagram, can be seen here https://miro.medium.com/v2/resize:fit:720/format:webp/1*C_GvfoFe486wT_D7UixhOw.png

### EKS
- EKS put in app subnets
- In this example, only use the provided policies. In real scenario, if there is possibility to do least privilege permissions, its advised to do it if possible.
- In this example, provided custom ingress fields, the implementation using in real scenario should be least privileges IPs (VPN, whitelisted IP)
- The module use public endpoint access, for more secure, its advised to use private endpoint access
- EKS cluster module addons are default provided, no changes on this

### Kubernetes Deployment
- Not done this yet.
- Planned to use ArgoCD to make it easier on application management (use Application API) and also replicating it to multiple cluster (using ApplicationSets API)
- This requires creating Helm chart for the app before deploy it using Application API
- References:
  - https://argo-cd.readthedocs.io/en/stable/user-guide/helm/
  - https://argo-cd.readthedocs.io/en/stable/operator-manual/architecture/
  - https://argo-cd.readthedocs.io/en/stable/operator-manual/installation/#helm

### IAC
- Using opentofu / terraform
- Using local state, should use remote state in best practice like S3
- Using module to create VPC and EKS

### DB
- Not done this yet.
- Use RDS in DB subnet
- Ingress from the cluster SG

### CI/CD
- Not done this yet.
- Planned stages are:
  - Build
    - Lint using ESLint (npx eslint packages)
    - Build using Dockerfile
  - Test
    - spin the container using Docker Compose with DIND
    - ran the npm test within the container, get the results
  - Publish
    - Publish the image using the Dockerfile to the registry
  - Deploy
    - Run kubetl apply upon the ArgoCD application manifest

# ![RealWorld Example App](logo.png)

> ### Ditsmod codebase containing real world examples (CRUD, auth, i18n, OpenAPI with validation, etc) that adheres to the [RealWorld](https://github.com/gothinkster/realworld) spec and API.


This codebase was created to demonstrate a fully fledged fullstack application built with **Ditsmod** including CRUD operations, authentication, routing, pagination, and more.

## Prerequisites

Please make sure that Node.js >= v20.6.0 is installed on your operating system.

## Getting started

This monorepository includes [Ditsmod](https://ditsmod.github.io/en/) applications seed. Packages are in ESM format and have [native Node.js aliases](https://nodejs.org/api/packages.html#subpath-imports) starting with `#`.

All packages are located in `packages/*` directory.

From start you need:

1. Clone the project

```bash
git clone https://github.com/ditsmod/realworld.git my-app
cd my-app
```

2. Bootstrap the project

```bash
npm install
```

3. Copy `packages/server/.env-example` to `packages/server/.env`:

```bash
cp packages/server/.env-example packages/server/.env
```

And fill this file.

4. Then create database (for example `real_world`), grant access permissions for this database, and execute `MySQL`-dump from [packages/server/sql/dump/info.sql](./packages/server/sql/dump/info.sql).

## Start the web server

```bash
npm start
```

After that, see OpenAPI docs on [http://0.0.0.0:3000/api/openapi](http://0.0.0.0:3000/api/openapi) and paste "http://0.0.0.0:3000/api/openapi.json" (or *.yaml) in the field above.

## Postman tests

To run [postman tests](https://github.com/gothinkster/realworld/blob/main/api/Conduit.postman_collection.json):

```bash
npm test
```
