terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.52.0"
    }
  }
}

module "develop_vpc" {
  source         = "../modules/vpc"
  vpc_name       = "develop-vpc"
  vpc_cidr_block = "172.16.0.0/16"
  common_tags = {
    environment = "develop"
    owner       = "infrastructure"
  }
  public_subnet_configs = {
    "public-subnet-a" = {
      subnet_cidr_block        = "172.16.1.0/24"
      subnet_availability_zone = "us-east-1a"
    }

    "public-subnet-b" = {
      subnet_cidr_block        = "172.16.2.0/24"
      subnet_availability_zone = "us-east-1b"
    }
  }

  app_subnet_configs = {
    "app-subnet-a" = {
      subnet_cidr_block        = "172.16.3.0/24"
      subnet_availability_zone = "us-east-1a"
    }

    "app-subnet-b" = {
      subnet_cidr_block        = "172.16.4.0/24"
      subnet_availability_zone = "us-east-1b"
    }
  }

  db_subnet_configs = {
    "db-subnet-a" = {
      subnet_cidr_block        = "172.16.5.0/24"
      subnet_availability_zone = "us-east-1a"
    }

    "db-subnet-b" = {
      subnet_cidr_block        = "172.16.6.0/24"
      subnet_availability_zone = "us-east-1b"
    }
  }
}

module "develop-eks" {
  source             = "../modules/eks"
  cluster_name       = "develop-eks"
  cluster_subnet_ids = module.develop_vpc.app_subnet_ids
  cluster_vpc_id     = module.develop_vpc.vpc_id
  cluster_ingress_rules = {
    # for sample, it has to be specific sets of whitelisted IP instead
    # or even better use VPN and private cluter endpoint in EKS
    allow_all = {
      protocol    = "tcp"
      from_port   = 0
      to_port     = 0
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
}