output "app_subnet_ids" {
  value = [
    for key, value in aws_subnet.private_app : value.id
  ]
}

output "vpc_id" {
  value = aws_vpc.this.id
}
