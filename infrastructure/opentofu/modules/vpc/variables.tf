variable "vpc_name" {
  type        = string
  description = "The name of the new VPC"
}

variable "vpc_cidr_block" {
  type        = string
  description = "The CIDR block for the new VPC"
}

variable "enable_dns_hostnames" {
  type        = bool
  description = "The toggle to enable/disable the DNS hostname in VPC"
  default     = true
}

variable "common_tags" {
  type        = map(string)
  description = "common tags for the created VPC resources"
  default     = {}
}

variable "public_subnet_configs" {
  type = map(object({
    subnet_cidr_block        = string
    subnet_availability_zone = string
  }))
  description = "public subnet configs to be created in new VPC"
  default     = {}
}

variable "app_subnet_configs" {
  type = map(object({
    subnet_cidr_block        = string
    subnet_availability_zone = string
  }))
  description = "app subnet configs to be created in new VPC"
  default     = {}
}

variable "db_subnet_configs" {
  type = map(object({
    subnet_cidr_block        = string
    subnet_availability_zone = string
  }))
  description = "app subnet configs to be created in new VPC"
  default     = {}
}