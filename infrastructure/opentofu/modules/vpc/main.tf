resource "aws_vpc" "this" {
  cidr_block           = var.vpc_cidr_block
  enable_dns_hostnames = var.enable_dns_hostnames
  tags = merge(
    zipmap(["Name"], [var.vpc_name]),
    var.common_tags
  )
}

resource "aws_internet_gateway" "this" {
  vpc_id = aws_vpc.this.id
  tags   = var.common_tags
}

# Route Tables
# Public
resource "aws_route_table" "public" {
  vpc_id = aws_vpc.this.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.this.id
  }

  tags = merge(
    zipmap(["Name"], ["public-rtb"]),
    var.common_tags
  )
}

# Private
resource "aws_route_table" "private" {
  vpc_id = aws_vpc.this.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.this.id
  }

  tags = merge(
    zipmap(["Name"], ["private-rtb"]),
    var.common_tags
  )
}

# NAT - just use the first subnet created for the NAT GW for this sample
resource "aws_nat_gateway" "this" {
  allocation_id = aws_eip.nat.id
  subnet_id     = aws_subnet.public[keys(aws_subnet.public)[0]].id
}

resource "aws_eip" "nat" {
  domain = "vpc"
  tags = merge(
    zipmap(["Name"], ["private-nat-eip"]),
    var.common_tags
  )
}

# Subnets
# Public
resource "aws_subnet" "public" {
  for_each = var.public_subnet_configs

  vpc_id                  = aws_vpc.this.id
  cidr_block              = each.value.subnet_cidr_block
  availability_zone       = each.value.subnet_availability_zone
  map_public_ip_on_launch = true

  tags = merge(
    zipmap(["Name"], [each.key]),
    var.common_tags
  )
}

resource "aws_route_table_association" "public_rtb" {
  for_each       = aws_subnet.public
  subnet_id      = each.value.id
  route_table_id = aws_route_table.public.id
}

# Private
resource "aws_subnet" "private_app" {
  for_each = var.app_subnet_configs

  vpc_id                  = aws_vpc.this.id
  cidr_block              = each.value.subnet_cidr_block
  availability_zone       = each.value.subnet_availability_zone
  map_public_ip_on_launch = false

  tags = merge(
    zipmap(["Name"], [each.key]),
    var.common_tags
  )
}

resource "aws_subnet" "private_db" {
  for_each = var.db_subnet_configs

  vpc_id                  = aws_vpc.this.id
  cidr_block              = each.value.subnet_cidr_block
  availability_zone       = each.value.subnet_availability_zone
  map_public_ip_on_launch = false

  tags = merge(
    zipmap(["Name"], [each.key]),
    var.common_tags
  )
}

resource "aws_route_table_association" "private_app_rtb" {
  for_each       = aws_subnet.private_app
  subnet_id      = each.value.id
  route_table_id = aws_route_table.private.id
}

resource "aws_route_table_association" "private_db_rtb" {
  for_each       = aws_subnet.private_app
  subnet_id      = each.value.id
  route_table_id = aws_route_table.private.id
}