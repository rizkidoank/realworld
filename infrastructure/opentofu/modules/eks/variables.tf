variable "cluster_name" {
  type        = string
  description = "the cluster name for the EKS cluster"
}

variable "cluster_vpc_id" {
  type        = string
  description = "the vpc id for the EKS cluster"
}

variable "cluster_subnet_ids" {
  type        = list(string)
  description = "the subnets for the EKS cluster"
}

variable "cluster_ingress_rules" {
  type = map(object({
    protocol    = string
    from_port   = number
    to_port     = number
    cidr_blocks = list(string)
  }))
  description = "the ingress rules for the EKS cluster"
}


variable "common_tags" {
  type        = map(string)
  description = "common tags for the created VPC resources"
  default     = {}
}